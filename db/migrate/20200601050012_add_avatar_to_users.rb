# frozen_string_literal: true

class AddAvatarToUsers < ActiveRecord::Migration[6.0]
  def change
    change_table :users, bulk: true do |t|
      t.string :avatar
    end
  end
end
