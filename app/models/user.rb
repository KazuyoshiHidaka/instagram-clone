# frozen_string_literal: true

class User < ApplicationRecord
  has_many :followers, class_name: 'Follow', foreign_key: :following_id, dependent: :destroy
  has_many :followings, class_name: 'Follow', foreign_key: :follower_id, dependent: :destroy
  # has_many :followers, through: :follows, source: :follower
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: %i[facebook]
  mount_uploader :avatar, AvatarUploader

  def self.from_omniauth(auth)
    user = find_or_initialize_by(provider: auth.provider, uid: auth.uid)
    user.email = auth.info.email
    user.password = Devise.friendly_token[0, 20]
    user.name = auth.info.name
    user.remote_avatar_url = auth.info.image # Facebook認証の場合remote_avatar_url
    # If you are using confirmable and the provider(s) you use validate emails,
    # uncomment the line below to skip the confirmation emails.
    # user.skip_confirmation!

    user.save
    user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      data = session['devise.facebook_data']
      if data && data['extra']['raw_info']
        user.email = data['email'] if user.email.blank?
      end
    end
  end

  def follow(following_id)
    followings.create(following_id: following_id)
  end

  def unfollow(following_id)
    followings.find_by(following_id: following_id)&.destroy
  end

  def following_ids_in(ids)
    followings.where(following_id: ids).pluck(:following_id)
  end
end
