## テストユーザー

- email: test@test.com
- password: password

## 自分が苦労した点

- docker 上への環境構築

## 学んだ点

- RSpec の書き方
- 画像のバージョン管理, リサイズ処理の設定
- ブラウザテストの導入方法
