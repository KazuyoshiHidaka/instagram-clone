# frozen_string_literal: true

class UsersController < ApplicationController
  def index
    @users = User.where.not(id: current_user.id).page(params[:page])
    @ids_current_user_followings = current_user.following_ids_in @users.ids
  end

  def show
    @user = User.find(params[:id])
  end
end
