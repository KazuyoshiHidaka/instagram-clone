# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'フォロー機能' do
    let(:user_follower)  { create(:user) }
    let(:user_following) { create(:user) }
    let(:follow) { user_follower.follow(user_following.id) }

    describe '#follow' do
      it 'フォローできている' do
        follow
        expect(Follow.find_by(follower_id: user_follower.id, following_id: user_following.id).present?).to be true
      end

      it '返り値がFollowインスタンス' do
        expect(follow.instance_of?(Follow)).to be true
      end
    end

    describe '#unfollow' do
      before { follow }

      let(:unfollow) { user_follower.unfollow(user_following.id) }

      it 'フォロー解除できている' do
        unfollow
        expect(Follow.exists?).to be false
      end

      it '返り値がFollowインスタンス' do
        expect(follow.instance_of?(Follow)).to be true
      end
    end
  end
end
