# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Follow, type: :model do
  let(:user_follower) { create(:user) }
  let(:user_following) { create(:user) }

  describe 'フォローするとき' do
    let(:follow) { user_follower.followings.create(following_id: user_following.id) }

    context '通常の場合' do
      it '成功する' do
        expect(Follow.exists?(follow.id)).to be true
      end
    end

    context '自分をフォローした場合' do
      let(:follow_self) { user_follower.followings.create(following_id: user_follower.id) }

      it '失敗する' do
        expect(Follow.exists?(follow_self.id)).to be false
      end
    end

    context '既にフォローしているユーザーをフォローした場合' do
      let(:follow_duplicated) { user_follower.followings.create(following_id: user_following.id) }

      it '失敗する' do
        follow
        expect(Follow.exists?(follow_duplicated.id)).to be false
      end
    end
  end
end
