# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FollowsController, type: :request do
  login_user

  let(:following_user) { create(:user) }

  describe 'POST #create' do
    context 'フォローが成功する場合' do
      it 'HTTPステータスが204' do
        post '/follows', params: { follow: { following_id: following_user.id }, format: :js }
        expect(response).to render_template :create
      end
    end

    context 'フォローが失敗する場合' do
      it 'ルートにリダイレクト' do
        post '/follows', params: { follow: { following_id: nil } }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      post '/follows', params: { follow: { following_id: following_user.id }, format: :js }
    end

    context 'フォロー解除が成功する場合' do
      it 'HTTPステータスが204' do
        delete "/follows/#{following_user.id}", params: { follow: { following_id: following_user.id }, format: :js }
        expect(response).to render_template :destroy
      end
    end

    context 'フォロー解除が失敗する場合' do
      it 'ルートにリダイレクト' do
        delete  "/follows/#{following_user.id + 1}",
                params: { follow: { following_id: following_user.id + 2 }, format: :js }
        expect(response).to redirect_to root_path
      end
    end
  end
end
