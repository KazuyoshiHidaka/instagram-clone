# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

30.times do
  image = if Rails.env.production?
            Faker::Avatar.image(size: '50x50')
          else
            Faker::Placeholdit.image(size: '50x50')
          end
  User.create(
    email: Faker::Internet.unique.safe_email,
    password: Faker::Internet.password(min_length: 6),
    name: Faker::Name.name,
    remote_avatar_url: image
  )
end
