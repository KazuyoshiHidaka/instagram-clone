# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Follows', type: :feature do
  let(:current_user) { create(:user) }
  let(:following_user) { create(:user) }

  before do
    sign_in current_user
    following_user
  end

  scenario 'Follow', js: true, driver: :selenium do
    visit '/'
    btn_selector = "#follow-form-#{following_user.id} input[type='submit']"

    btn = find(btn_selector)
    btn.click
    expect(page).to have_selector "#{btn_selector}[value='Following']"
    expect(Follow.count).to eq 1

    btn.click
    expect(page).to have_selector "#{btn_selector}[value='Follow']"
    expect(Follow.count).to eq 0
  end

  feature 'Users#show画面でのFollowボタンの表示' do
    context 'current_userのshow画面の場合' do
      scenario 'フォローボタンが表示されない' do
        selector = "#follow-form-#{current_user.id}"

        visit "/users/#{current_user.id}"
        expect(page).not_to have_selector selector
      end
    end

    context 'current_user以外のshow画面の場合' do
      scenario 'フォローボタンが表示される' do
        selector = "#follow-form-#{following_user.id}"

        visit "/users/#{following_user.id}"
        expect(page).to have_selector selector
      end
    end
  end
end
