# frozen_string_literal: true

# following: フォローされる方, follower: フォローする方

class Follow < ApplicationRecord
  belongs_to :follower, class_name: 'User'
  belongs_to :following, class_name: 'User'

  validates :follower_id, presence: true
  validates :following_id, presence: true
  validate :user_cannot_follow_self
  validate :both_ids_combination_cannot_duplicate

  def user_cannot_follow_self
    errors.add(:following_id, '自分をフォローすることはできません') if follower_id == following_id
  end

  def both_ids_combination_cannot_duplicate
    follow = Follow.find_by(follower_id: follower_id, following_id: following_id)
    errors.add(:following_id, '既にフォローしているユーザーです') if follow
  end
end
