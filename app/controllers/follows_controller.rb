# frozen_string_literal: true

class FollowsController < ApplicationController
  def create
    follow = current_user.follow(following_id)
    if Follow.exists?(follow.id)
      @following_id = following_id
      render format: :js
    else
      flash[:danger] = 'フォローに失敗しました'
      redirect_to root_path

    end
  end

  def destroy
    unfollow = current_user.unfollow(following_id)
    if unfollow
      @following_id = following_id
      render format: :js
    else
      flash[:danger] = 'フォロー解除に失敗しました'
      redirect_to root_path
    end
  end

  private

  def following_id
    params.require(:follow)[:following_id]
  end
end
