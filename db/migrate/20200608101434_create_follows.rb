# frozen_string_literal: true

class CreateFollows < ActiveRecord::Migration[6.0]
  def change
    create_table :follows, { bulk: true } do |t|
      t.belongs_to :following, foreign_key: { to_table: :users }
      t.belongs_to :follower, foreign_key: { to_table: :users }
      t.timestamps

      t.index %i[following_id follower_id], unique: true
    end
  end
end
